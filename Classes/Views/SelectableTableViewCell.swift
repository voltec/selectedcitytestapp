//
//  SelectableTableViewCell.swift
//  AudiomolitvoslovLibrary
//
//  Created by Mukhail Mukminov on 18.05.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import UIKit

class SelectableTableViewCell: UITableViewCell {
    
    @IBInspectable var selectionColor: UIColor = UIColor.grayColor() {
        didSet {
            configureSelectedBackgroundView()
        }
    }
    
    func configureSelectedBackgroundView() {
        let view = UIView()
        view.backgroundColor = selectionColor
        selectedBackgroundView = view
    }
    
}