//
//  CityTableViewCell.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 09.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import UIKit

class CityTableViewCell: SelectableTableViewCell {
    
    @IBOutlet private weak var cityNameLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!
    
    var cellSelected: Bool = false

    var favoriteClick: (()->())?
    
    var city: CityRepresenting! {
        didSet {
            cityNameLabel.text = city.name
            favoriteButton.selected = city.isFavorite
        }
    }
}

//Actions
extension CityTableViewCell {
    
    @IBAction func favoriteClick(sender: UIButton) {
        favoriteClick?()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        if selected == cellSelected {
            super.setSelected(selected, animated: animated)
        }
    }
    
}
