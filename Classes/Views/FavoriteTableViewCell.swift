//
//  FavoriteTableViewCell.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 09.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var cityNameLabel: UILabel!
    
    var deleteClick: (()->())?
    
    var city: CityRepresenting! {
        didSet {
            cityNameLabel.text = city.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

//Actions
extension FavoriteTableViewCell {
    
    @IBAction func deleteClick(sender: UIButton) {
        deleteClick?()
    }
    
}