//
//  CountryTableViewCell.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 09.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    var country: CountryRepresenting! {
        didSet {
            textLabel?.text = country.name
        }
    }

}
