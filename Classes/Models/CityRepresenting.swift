//
//  CityRepresenting.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 08.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import Foundation

@objc protocol CityRepresenting: Representing {
    
    var id: Int { get }
    var name: String! { get }
    var imageUrl: NSURL? { get }
    
    var country: CountryRepresenting! { get }
    
    var isFavorite: Bool { get set }
    
}

