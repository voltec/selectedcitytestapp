//
//  City+CoreDataProperties.swift
//  
//
//  Created by Mukhail Mukminov on 08.06.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension City {

    @NSManaged var mIdentifier: NSNumber?
    @NSManaged var mName: String?
    @NSManaged var mPictureUrlString: String?
    @NSManaged var mFavorite: NSNumber?
    @NSManaged var relCountry: NSManagedObject?

}
