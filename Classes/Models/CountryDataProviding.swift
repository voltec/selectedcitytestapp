//
//  CountryDataProviding.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 09.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import Foundation

protocol CountryDataProviding: ListDataProviding {
    
    var selectedCity: CityRepresenting? { get set }
    
}