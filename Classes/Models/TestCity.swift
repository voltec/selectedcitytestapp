//
//  TestCity.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 08.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import UIKit

class TestCity: NSObject, CityRepresenting {
    
    var id: Int
    var name: String!
    var imageUrl: NSURL?
    
    var country: CountryRepresenting!
    
    var isFavorite: Bool = false
    
    init(id: Int, name: String, imageUrl: NSURL?) {
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
    }
}
