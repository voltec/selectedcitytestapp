//
//  CountryRepresenting.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 08.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import Foundation

@objc protocol CountryRepresenting: Representing {
    
    var id: Int { get }
    var name: String! { get }
    var imageUrl: NSURL? { get }
    
    var cities: [CityRepresenting] { get }
    
}