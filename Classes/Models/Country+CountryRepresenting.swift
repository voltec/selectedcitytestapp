//
//  Country+Representing.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 08.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import Foundation

extension Country: CountryRepresenting {
    
    var id: Int { return mIdentifier!.integerValue }
    var name: String! { return mName }
    var imageUrl: NSURL? { return mPictureUrlString != nil ? NSURL(string: mPictureUrlString!) : nil }
    
    var cities: [CityRepresenting] { return Array(relCities as! Set<City>) }
}