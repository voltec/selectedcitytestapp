//
//  City+CityRepresenting.swift
//  SelectedCityTestApp
//
//  Created by Mukhail Mukminov on 08.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

import Foundation

extension City: CityRepresenting {
    
    var id: Int { return mIdentifier!.integerValue }
    var name: String! { return mName }
    var imageUrl: NSURL? { return mPictureUrlString != nil ? NSURL(string: mPictureUrlString!) : nil }
    
    var country: CountryRepresenting! { return relCountry as! CountryRepresenting }
    
    var isFavorite: Bool {
        get { return mFavorite!.boolValue }
        set {
            mFavorite = newValue
            self.managedObjectContext?.MR_saveToPersistentStoreWithCompletion(nil)
        }
    }
    
}