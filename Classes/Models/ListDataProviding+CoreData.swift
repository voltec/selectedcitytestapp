//
//  ListDataProviding+CoreData.swift
//  NexText
//
//  Created by Daniil Konoplev on 08/12/15.
//  Copyright © 2015 NexText Team. All rights reserved.
//

import Foundation
import CoreData

extension NSFetchedResultsChangeType {
    
    var listDataProviderChangeType: ListDataProviderChangeType {
        switch self {
        case .Update:
            return .Update
        case .Insert:
            return .Insert
        case .Delete:
            return .Delete
        case .Move:
            return .Move
        }
    }
    
}